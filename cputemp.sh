#!/bin/bash
cputemp=$(</sys/class/thermal/thermal_zone0/temp)
echo "$(date) @ $(hostname)"
echo "-------------------------------------------------------------------------"
echo "CPU temperature = $((cputemp/1000))'C"
