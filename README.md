# IOT-RPI-system
These files are used to setup the HAN-IoT-RPI-SD-card.

# Getting started

Follow these steps to update and install all dependecies: When in doubt, type Y(es).

1. Download the latest raspbian image for Raspberry Pi from: https://www.raspberrypi.com/software/. Select one of the following images 
    - Raspberry Pi OS with desktop and recommended software; Complete version inclusive desktop
    - Raspberry Pi OS Lite; minimal version without desktop. Addition software for IoT class needs to be in installed when required.
2. Either install Raspberry Pi OS using Raspberry Pi Imager or manually to your uSD-card.
3. Start the Raspberry with a display, keyboard and mouse connected.
4. To enable using the Raspberry Pi 4 to boot without HDMI plugged in (headless) add "hdmi_force_hotplug=1" to file /boot/config.txt (need root rights)

# Add IoT additions the the fresh installed image

Follow these steps:

1. Clone the repository from https://gitlab.com/wlgrw/iot-rpi-system 
2. Initialize and repositories that are linked in to this repository. From teh commandline execute ```git submodule update --init```.
2. Copy the content of the respository to the home directory of the user pi.
3. Run the script ```installRPI.sh``` with root previledges.
3. Install ```startupSenseHAT.py``` and ```bootCount.sh``` in Crontab (see below) 

## Crontab

Add these lines to crontab using ```crontab -e```:

```
@reboot /home/pi/startupSenseHAT.py
@reboot /home/pi/bootCount.sh
```
# Description of files

These will be added later.

# Disclaimer
This repository is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; Without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
