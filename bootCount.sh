#!/bin/bash

# Counts the number of booting the RPI
# Started by @reboot in crontab (list crontab: crontab -l )

if [ ! -f .bootCount.txt ]
   then
      echo 0 > .bootCount.txt
fi

count=$(cat .bootCount.txt)
count=$((count+1))

echo $count > .bootCount.txt

