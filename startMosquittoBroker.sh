#!/bin/bash
# File: mosquittoBroker.sh

version="1.0"

# Getting hostname from /etc/hostname
if [ -f /etc/hostname ]; then
   host=$(</etc/hostname)
else
   host="???"
fi

ipaddress=$(hostname -I)

# Make sure only root can run this script
echo "-- Start Mosquitto MQTT broker v$version"
echo "-- Hostname $host  $ipaddress"
echo

if [[ $EUID -ne 0 ]]; then
   echo "** This script must be run as root" 1>&2
   exit 1
fi

mosquitto -c /etc/mosquitto/mosquitto.conf

